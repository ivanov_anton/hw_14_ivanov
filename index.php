<?php
	require_once 'input_data.php';
	require_once 'searcherWord.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="bootstrap.css">
    <title>Document</title>
</head>
<body class="bg-light">
	<div class="container">
		<nav class="navbar navbar-dark bg-dark navbar-expand">
			<a href="index.php" class="navbar-brand"><span class="badge badge-info">Logo.com</span></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<a href="index.php" class="nav-item nav-link">Main page</a>
					<a href="#" class="nav-item nav-link">Maps</a>
					<a href="#" class="nav-item nav-link">About us</a>
					<a href="#" class="nav-item nav-link">Contact</a>
				</ul>
			</div>
		</nav>
		<div class="row">
			<h2 class="p-2 col-12 text-center text-black-50">Welcome the us new application</h2>
			<form action="" class="col-12">
				<div class="form-group row ">
					<input type="text" class="form-control col-6 mx-auto active" name="words" value="">
				</div>
			</form>
		</div>
		<i class="text-info">Написать функцию, которая примет эти массивы как аргументы и в цикле с помощью регулярных выражений проверит какие слова из массива searchWords содержатся в предложениях массива $searcStrings. Результат должен быть вида:
			В предложении №1 есть слова: интернет.
			В предложении №2 есть слова: php.
			В предложении №3 есть слова: php, Web , html.
		</i><br>
		<hr>

		<?php
			searcher($searchWords, $searchStrings);
		?>

	</div>
</body>
</html>